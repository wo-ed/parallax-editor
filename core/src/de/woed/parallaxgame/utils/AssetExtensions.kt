package de.woed.parallaxgame.utils

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import ktx.assets.ignore
import ktx.assets.unloadSafely
import ktx.collections.GdxMap
import ktx.collections.gdxListOf
import ktx.collections.toGdxMap

fun <T> AssetManager.getOrNull(descriptor: AssetDescriptor<T>): T? = try {
    get(descriptor)
} catch (e: Exception) {
    e.ignore()
    null
}

fun AssetManager.load(assets: Iterable<AssetDescriptor<*>>) = assets.forEach { load(it) }
fun AssetManager.load(vararg assets: AssetDescriptor<*>) = assets.forEach { load(it) }
fun Iterable<AssetDescriptor<*>>.load(assetManager: AssetManager) = forEach { assetManager.load(it) }

fun AssetManager.unloadSafely(descriptor: AssetDescriptor<*>) = unloadSafely(descriptor.fileName)
fun AssetManager.unloadSafely(assets: Iterable<AssetDescriptor<*>>) = assets.forEach { unloadSafely(it) }
fun AssetManager.unloadSafely(vararg assets: AssetDescriptor<*>) = assets.forEach { unloadSafely(it) }
fun Iterable<AssetDescriptor<*>>.unloadSafely(assetManager: AssetManager) = forEach { assetManager.unloadSafely(it) }

fun AssetManager.isLoaded(assets: Iterable<AssetDescriptor<*>>) = assets.all { isLoaded(it.fileName, it.type) }
fun AssetManager.isLoaded(vararg assets: AssetDescriptor<*>) = assets.all { isLoaded(it.fileName, it.type) }
fun Iterable<AssetDescriptor<*>>.isLoaded(assetManager: AssetManager) = all { assetManager.isLoaded(it) }

fun AssetManager.finishLoadingAsset(descriptor: AssetDescriptor<*>) = finishLoadingAsset(descriptor.fileName)
fun AssetManager.finishLoadingAssets(assets: Iterable<AssetDescriptor<*>>) = assets.forEach { finishLoadingAsset(it) }
fun AssetManager.finishLoadingAssets(vararg assets: AssetDescriptor<*>) = assets.forEach { finishLoadingAsset(it) }
fun Iterable<AssetDescriptor<*>>.finishLoadingAssets(assetManager: AssetManager) = forEach { assetManager.finishLoadingAsset(it) }

fun AssetManager.getAssetFileNames(assets: Iterable<*>): GdxMap<*, String?> {
    return assets.toGdxMap(keyProvider = { it }, valueProvider = { getAssetFileName(it) })
}

fun AssetManager.getAssetFileNames(vararg assets: Any): GdxMap<*, String?> {
    return assets.toGdxMap(keyProvider = { it }, valueProvider = { getAssetFileName(it) })
}

fun Iterable<AssetDescriptor<*>>.getAssetFileNames(assetManager: AssetManager): GdxMap<AssetDescriptor<*>, String?> {
    return toGdxMap(keyProvider = { it }, valueProvider = { assetManager.getAssetFileName(it) })
}

operator fun <T> AssetManager.get(descriptor: ConsumingAssetDescriptor<T>) = descriptor.get(this)

class AtlasAnimationDescriptor(
        val name: String,
        private val descriptor: AssetDescriptor<TextureAtlas>,
        private val frameDuration: Float,
        private val playMode: Animation.PlayMode = Animation.PlayMode.NORMAL
) : ConsumingAssetDescriptor<Animation<TextureAtlas.AtlasRegion>>(gdxListOf(descriptor)) {

    override fun get(assetManager: AssetManager): Animation<TextureAtlas.AtlasRegion> {
        val atlas = assetManager[descriptor]
        return atlas.findAnimation(name, frameDuration, playMode)!!
    }
}

class AtlasRegionDescriptor(val name: String, private val descriptor: AssetDescriptor<TextureAtlas>)
    : ConsumingAssetDescriptor<TextureAtlas.AtlasRegion>(gdxListOf(descriptor)) {

    override fun get(assetManager: AssetManager): TextureAtlas.AtlasRegion {
        val atlas = assetManager[descriptor]
        return atlas.findRegion(name)
    }
}

abstract class ConsumingAssetDescriptor<T>(val dependencies: Iterable<AssetDescriptor<*>>) {
    fun load(assetManager: AssetManager) = assetManager.load(dependencies)
    fun isLoaded(assetManager: AssetManager) = assetManager.isLoaded(dependencies)
    fun finishLoading(assetManager: AssetManager) = assetManager.finishLoadingAssets(dependencies)
    abstract fun get(assetManager: AssetManager): T
}