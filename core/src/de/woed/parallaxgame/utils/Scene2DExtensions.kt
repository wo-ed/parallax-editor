package de.woed.parallaxgame.utils

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor

fun Actor.hasFocus() = stage?.keyboardFocus === this

fun Actor.requireFocus(): Boolean = stage?.setKeyboardFocus(this) ?: false

fun Actor.toRect(rect: Rectangle): Rectangle = rect.set(x, y, width, height)