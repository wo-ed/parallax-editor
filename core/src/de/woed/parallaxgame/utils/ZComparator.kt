package de.woed.parallaxgame.utils

import com.badlogic.ashley.core.Entity
import de.woed.parallaxgame.components.transform

object AscendingZComparator : ZComparator(ascending = true)
object DescendingZComparator : ZComparator(ascending = false)

open class ZComparator(val ascending: Boolean = true) : Comparator<Entity> {
    override fun compare(e1: Entity, e2: Entity): Int {
        val z1 = e1.transform?.z ?: 0f
        val z2 = e2.transform?.z ?: 0f
        return if (ascending) z1.compareTo(z2) else z2.compareTo(z1)
    }
}