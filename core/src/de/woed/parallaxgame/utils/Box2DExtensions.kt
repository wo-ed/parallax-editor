package de.woed.parallaxgame.utils

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import ktx.collections.GdxFloatArray
import ktx.math.component1
import ktx.math.component2

private val tmpV = Vector2()
private val tmpArr = GdxFloatArray()
private val tmpList1 = mutableListOf<Float>()
private val tmpList2 = mutableListOf<Float>()

/** @throws UnsupportedOperationException for EdgeShape */
fun Fixture.size(v2: Vector2): Vector2 = shape.size(v2)

/** @throws UnsupportedOperationException for EdgeShape */
fun Shape.size(v2: Vector2): Vector2 {
    return when (this) {
        is CircleShape -> v2.set(radius * 2, radius * 2)
        is ChainShape -> {
            val vertices = this.vertices(tmpArr)
            return getSizeFromVertices(vertices, v2)
        }
        is PolygonShape -> {
            val vertices = this.vertices(tmpArr)
            return getSizeFromVertices(vertices, v2)
        }
        else -> throw UnsupportedOperationException()
    }
}

fun ChainShape.vertices(arr: GdxFloatArray): GdxFloatArray = getVertices(vertexCount, arr, ::getVertex)
fun PolygonShape.vertices(arr: GdxFloatArray): GdxFloatArray = getVertices(vertexCount, arr, ::getVertex)

private inline fun getVertices(
        vertexCount: Int,
        arr: GdxFloatArray,
        getVertex: (i: Int, v2: Vector2) -> Unit
): GdxFloatArray {

    arr.clear()
    for (i in 0 until vertexCount) {
        getVertex(i, tmpV)
        val (x, y) = tmpV
        arr.add(x)
        arr.add(y)
    }

    return arr
}

private fun getSizeFromVertices(vertices: GdxFloatArray, v2: Vector2): Vector2 {
    tmpList1.clear()
    tmpList2.clear()

    val even = vertices.items.filterIndexedTo(tmpList1) { i, _ -> i % 2 == 0 }
    val odd = vertices.items.filterIndexedTo(tmpList2) { i, _ -> i % 2 != 0 }
    val width = (even.max() ?: 0f) - (even.min() ?: 0f)
    val height = (odd.max() ?: 0f) - (odd.min() ?: 0f)
    return v2.set(width, height)
}