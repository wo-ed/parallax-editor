package de.woed.parallaxgame.utils

import com.badlogic.gdx.Input

enum class Dimension { X, Y }

val arrowKeyboardMovements = listOf(
        Triple(Input.Keys.LEFT, Dimension.X, -1),
        Triple(Input.Keys.RIGHT, Dimension.X, 1),
        Triple(Input.Keys.DOWN, Dimension.Y, -1),
        Triple(Input.Keys.UP, Dimension.Y, 1)
)

val wasdKeyboardMovements = listOf(
        Triple(Input.Keys.A, Dimension.X, -1),
        Triple(Input.Keys.D, Dimension.X, 1),
        Triple(Input.Keys.S, Dimension.Y, -1),
        Triple(Input.Keys.W, Dimension.Y, 1)
)