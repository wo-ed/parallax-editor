package de.woed.parallaxgame.utils

import com.badlogic.ashley.core.Entity
import de.woed.parallaxgame.components.box2d.bodyComponent

open class BodyDestroyer : EntityAdapter() {
    override fun entityRemoved(entity: Entity) {
        super.entityRemoved(entity)
        val bodyComponent = entity.bodyComponent ?: return
        if (bodyComponent.autoDestroy) {
            bodyComponent.destroyBody()
        }
    }

    companion object Instance : BodyDestroyer()
}