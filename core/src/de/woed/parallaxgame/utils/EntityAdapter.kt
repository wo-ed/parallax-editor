package de.woed.parallaxgame.utils

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener

open class EntityAdapter : EntityListener {
    override fun entityRemoved(entity: Entity) {}
    override fun entityAdded(entity: Entity) {}
}