package de.woed.parallaxgame.utils

import box2dLight.*
import com.badlogic.gdx.graphics.Color

const val defaultDistance = 15f
val defaultColor = Color(0.75f, 0.75f, 0.5f, 0.75f)

fun RayHandler.chainLight(
        rays: Int,
        color: Color = defaultColor,
        distance: Float = defaultDistance,
        rayDirection: Int = 0,
        chain: FloatArray? = null
) = ChainLight(this, rays, color, distance, rayDirection, chain)

fun RayHandler.coneLight(
        rays: Int,
        color: Color = defaultColor,
        distance: Float = defaultDistance,
        x: Float = 0f,
        y: Float = 0f,
        directionDegree: Float = 0f,
        coneDegree: Float = 45f
) = ConeLight(this, rays, color, distance, x, y, directionDegree, coneDegree)

fun RayHandler.directionalLight(
        rays: Int,
        color: Color = defaultColor,
        directionDegree: Float = 0f
) = DirectionalLight(this, rays, color, directionDegree)

fun RayHandler.pointLight(
        rays: Int,
        color: Color = defaultColor,
        distance: Float = defaultDistance,
        x: Float = 0f,
        y: Float = 0f
) = PointLight(this, rays, color, distance, x, y)