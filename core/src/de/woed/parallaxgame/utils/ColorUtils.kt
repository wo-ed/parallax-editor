package de.woed.parallaxgame.utils

import com.badlogic.gdx.graphics.Color

fun color(r: Int = 0, g: Int = 0, b: Int = 0, a: Int = 255) = Color(
        r / 255f,
        g / 255f,
        b / 255f,
        a / 255f
)

fun Color.toAwtColor() = java.awt.Color(
        (r * 255).toInt(),
        (g * 255).toInt(),
        (b * 255).toInt(),
        (a * 255).toInt()
)

fun java.awt.Color.toGdxColor() = Color(
        red / 255f,
        green / 255f,
        blue / 255f,
        alpha / 255f
)