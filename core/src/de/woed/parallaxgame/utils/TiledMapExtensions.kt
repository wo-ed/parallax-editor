package de.woed.parallaxgame.utils

import com.badlogic.gdx.maps.MapObject
import com.badlogic.gdx.maps.tiled.TiledMap

val TiledMap.width: Int? get() = properties["width", null]
val TiledMap.height: Int? get() = properties["height", null]
val TiledMap.tileWidth: Int? get() = properties["tilewidth", null]
val TiledMap.tileHeight: Int? get() = properties["tileheight", null]

val MapObject.width: Int? get() = properties["width", null]
val MapObject.height: Int? get() = properties["height", null]