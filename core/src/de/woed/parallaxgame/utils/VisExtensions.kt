package de.woed.parallaxgame.utils

import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.kotcrab.vis.ui.FocusManager
import com.kotcrab.vis.ui.Focusable
import com.kotcrab.vis.ui.widget.VisValidatableTextField
import ktx.vis.DEFAULT_STYLE
import ktx.vis.KVisTable
import ktx.vis.VisDsl

fun hasFocus(focusable: Focusable?) = FocusManager.getFocusedWidget() === focusable

inline fun KVisTable.floatField(initialValue: Float = 0.0f, requireValue: Boolean = false, styleName: String = DEFAULT_STYLE, crossinline init: (@VisDsl VisValidatableTextField).(Cell<*>) -> Unit = {}): VisValidatableTextField {
    return validatableTextField(initialValue.toString(), styleName) { cell ->
        addValidator { input: String ->
            val floatInput = input.toFloatOrNull()
            floatInput != null || !requireValue && input.isEmpty()
        }
        init(cell)
    }
}