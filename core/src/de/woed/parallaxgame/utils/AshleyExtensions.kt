package de.woed.parallaxgame.utils

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem

fun Engine.addSystems(vararg systems: EntitySystem) = systems.forEach { addSystem(it) }
fun Engine.addSystems(systems: Iterable<EntitySystem>) = systems.forEach { addSystem(it) }
operator fun Engine.plusAssign(system: EntitySystem) = addSystem(system)
operator fun Engine.plusAssign(systems: Iterable<EntitySystem>) = addSystems(systems)

//fun Engine.removeSystems(vararg systems: EntitySystem) = systems.forEach { removeSystem(it) }
//fun Engine.removeSystems(systems: Iterable<EntitySystem>) = systems.forEach { removeSystem(it) }
//operator fun Engine.minusAssign(system: EntitySystem) = removeSystem(system)
//operator fun Engine.minusAssign(systems: Iterable<EntitySystem>) = removeSystems(systems)

fun Engine.addEntities(vararg entities: Entity) = entities.forEach { addEntity(it) }
fun Engine.addEntities(entities: Iterable<Entity>) = entities.forEach { addEntity(it) }

//fun Engine.removeEntities(vararg entities: Entity) = entities.forEach { removeEntity(it) }
//fun Engine.removeEntities(entities: Iterable<Entity>) = entities.forEach { removeEntity(it) }

fun Entity.add(vararg components: Component) = components.forEach { add(it) }
fun Entity.add(components: Iterable<Component>) = components.forEach { add(it) }

operator fun Entity.plusAssign(component: Component) {
    add(component)
}

operator fun Entity.plusAssign(components: Iterable<Component>) = add(components)
//
//fun Entity.remove(componentClasses: Iterable<KClass<out Component>>) {
//    componentClasses.forEach {
//        remove(it.java)
//    }
//}

//fun Entity.remove(vararg componentClasses: KClass<out Component>) {
//    componentClasses.forEach {
//        remove(it.java)
//    }
//}
//
//operator fun Entity.minusAssign(componentClass: KClass<out Component>) {
//    remove(componentClass.java)
////}
//
//operator fun Entity.minusAssign(componentClasses: Iterable<KClass<out Component>>) {
//    remove(componentClasses)
//}