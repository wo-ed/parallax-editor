package de.woed.parallaxgame.utils

import com.badlogic.gdx.math.MathUtils

fun degToRad(deg: Float) = deg * MathUtils.degreesToRadians
fun radToDeg(rad: Float) = rad * MathUtils.radiansToDegrees