package de.woed.parallaxgame.utils

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.ContactImpulse
import com.badlogic.gdx.physics.box2d.ContactListener
import com.badlogic.gdx.physics.box2d.Manifold
import de.woed.parallaxgame.components.box2d.contactCountComponent

interface CountingContactListener : ContactListener {
    override fun beginContact(contact: Contact) {
        (contact.fixtureA.userData as? Entity)?.contactCountComponent?.apply { contactCount++ }
        (contact.fixtureB.userData as? Entity)?.contactCountComponent?.apply { contactCount++ }
    }

    override fun endContact(contact: Contact) {
        (contact.fixtureA.userData as? Entity)?.contactCountComponent?.apply { contactCount-- }
        (contact.fixtureB.userData as? Entity)?.contactCountComponent?.apply { contactCount-- }
    }
}

open class CountingContactAdapter : CountingContactListener {
    override fun preSolve(contact: Contact, oldManifold: Manifold) {}
    override fun postSolve(contact: Contact, impulse: ContactImpulse) {}

    companion object Instance : CountingContactAdapter()
}