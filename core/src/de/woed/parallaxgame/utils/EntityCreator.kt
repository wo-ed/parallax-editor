package de.woed.parallaxgame.utils

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import de.woed.parallaxgame.components.*
import de.woed.parallaxgame.components.box2d.BodyComponent
import ktx.ashley.create
import ktx.ashley.entity
import ktx.box2d.body
import ktx.math.component1
import ktx.math.component2

fun Engine.boxSpriteEntity(
        world: World,
        x: Float = 0f,
        y: Float = 0f,
        boxWidth: Float,
        boxHeight: Float,
        region: TextureRegion,
        spriteWidth: Float = boxWidth,
        spriteHeight: Float = boxHeight
): Entity {
    val body = world.body {
        box(boxWidth, boxHeight)
        position.set(x, y)
    }

    return bodySpriteEntity(body, region, spriteWidth, spriteHeight)
}

fun Engine.bodySpriteEntity(
        body: Body,
        region: TextureRegion,
        spriteWidth: Float = region.regionWidth.toFloat(),
        spriteHeight: Float = region.regionHeight.toFloat()
): Entity {
    val (x, y) = body.position

    return spriteEntity(region, x, y, spriteWidth, spriteHeight).apply {
        spriteSyncComponent?.apply {
            offsetX = -spriteWidth / 2f
            offsetY = -spriteHeight / 2f
        }

        this += create<BodyComponent> {
            this.body = body
        }
    }
}

fun Engine.parallaxEntity(
        region: TextureRegion,
        x: Float = 0f,
        y: Float = 0f,
        width: Float = region.regionWidth.toFloat(),
        height: Float = region.regionHeight.toFloat(),
        parallaxX: Float = 1f,
        parallaxY: Float = 1f
) = spriteEntity(region, x, y, width, height).apply {
    this += create<ProjectionComponent> {}
    this += create<ParallaxComponent> {
        this.factorX = parallaxX
        this.factorY = parallaxY
    }
}

fun Engine.parallaxEntity(
        sprite: Sprite? = null,
        parallaxX: Float = 1f,
        parallaxY: Float = 1f
) = spriteEntity(sprite).apply {
    this += create<ProjectionComponent> {}
    this += create<ParallaxComponent> {
        this.factorX = parallaxX
        this.factorY = parallaxY
    }
}

fun Engine.spriteEntity(
        region: TextureRegion,
        x: Float = 0f,
        y: Float = 0f,
        width: Float = region.regionWidth.toFloat(),
        height: Float = region.regionHeight.toFloat()
) = spriteEntity().apply {
    transform?.apply {
        this.x = x
        this.y = y
    }
    sprite?.apply {
        setRegion(region)
        setBounds(x, y, width, height)
        setOriginCenter()
    }
}

fun Engine.spriteEntity(sprite: Sprite? = null) = entity {
    with<TransformComponent> {}
    with<SpriteSyncComponent> {}
    with<SpriteComponent> {
        sprite?.let { this.sprite.set(it) }
    }
}