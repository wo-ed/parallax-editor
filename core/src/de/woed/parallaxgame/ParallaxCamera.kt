package de.woed.parallaxgame

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3

class ParallaxCamera : OrthographicCamera() {
    private val parallaxView = Matrix4()
    private val parallaxCombined = Matrix4()

    private val tmpV = Vector3()
    private val tmpV2 = Vector3()
    private val tmpM = Matrix4()

    fun calculateParallaxMatrix(parallaxX: Float, parallaxY: Float): Matrix4 {
        update()

        if (parallaxX == 1f && parallaxY == 1f) {
            return parallaxCombined.set(combined)
        }

        tmpV.apply {
            set(position)
            x *= parallaxX
            y *= parallaxY
        }

        parallaxView.setToLookAt(tmpV, tmpV2.set(tmpV).add(direction), up)
        parallaxCombined.set(projection)
        Matrix4.mul(parallaxCombined.values, parallaxView.values)
        return parallaxCombined
    }

    fun unprojectParallax(screenCoords: Vector3, viewportX: Float, viewportY: Float,
                          viewportWidth: Float, viewportHeight: Float,
                          parallaxX: Float, parallaxY: Float): Vector3 {

        val parallaxCombined = calculateParallaxMatrix(parallaxX, parallaxY)
        return unprojectParallax(screenCoords, viewportX, viewportY,
                viewportWidth, viewportHeight, parallaxCombined)
    }

    fun unprojectParallax(screenCoords: Vector3, viewportX: Float, viewportY: Float,
                          viewportWidth: Float, viewportHeight: Float,
                          parallaxCombined: Matrix4): Vector3 {
        val invParallaxCombined = tmpM.set(parallaxCombined).inv()

        return screenCoords.apply {
            x -= viewportX
            y = Gdx.graphics.height.toFloat() - y - 1f
            y -= viewportY

            x = 2 * x / viewportWidth - 1
            y = 2 * y / viewportHeight - 1
            z = 2 * z - 1

            prj(invParallaxCombined)
        }
    }
}