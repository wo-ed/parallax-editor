package de.woed.parallaxgame

import com.badlogic.gdx.Gdx
import ktx.inject.Context

interface ContextProvider {
    val context: Context
}

inline fun <reified T : Any> inject() = lazy {
    (Gdx.app.applicationListener as ContextProvider).context.inject<T>()
}