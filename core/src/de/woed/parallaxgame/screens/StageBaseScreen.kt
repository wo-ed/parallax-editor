package de.woed.parallaxgame.screens

import com.badlogic.gdx.InputMultiplexer
import de.woed.parallaxgame.inject

abstract class StageBaseScreen : BaseScreen(), StageScreen {
    override val inputMultiplexer: InputMultiplexer by inject()

    override fun render(delta: Float) {
        super<BaseScreen>.render(delta)
        super<StageScreen>.render(delta)
    }

    override fun resize(width: Int, height: Int) {
        super<BaseScreen>.resize(width, height)
        super<StageScreen>.resize(width, height)
    }

    override fun show() {
        super<BaseScreen>.show()
        super<StageScreen>.show()
    }

    override fun hide() {
        super<BaseScreen>.hide()
        super<StageScreen>.hide()
    }
}