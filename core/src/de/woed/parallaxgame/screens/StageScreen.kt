package de.woed.parallaxgame.screens

import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.Screen
import com.badlogic.gdx.scenes.scene2d.Stage

interface StageScreen : Screen {
    val stage: Stage
    val inputMultiplexer: InputMultiplexer?

    override fun render(delta: Float) {
        renderStage(delta)
    }

    fun renderStage(delta: Float) {
        stage.viewport.apply()
        stage.act(delta)
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height, true)
    }

    override fun show() {
        inputMultiplexer?.addProcessor(stage)
    }

    override fun hide() {
        inputMultiplexer?.removeProcessor(stage)
    }
}