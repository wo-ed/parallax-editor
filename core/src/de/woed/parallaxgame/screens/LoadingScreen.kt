package de.woed.parallaxgame.screens

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar
import com.badlogic.gdx.utils.viewport.ScreenViewport
import de.woed.parallaxgame.inject
import ktx.vis.table

class LoadingScreen : StageBaseScreen() {
    private val batch: Batch by inject()
    override val stage = Stage(ScreenViewport(), batch)

    private lateinit var progressBar: ProgressBar

    override fun initialize() {
        val rootTable = table {
            setFillParent(true)
            progressBar = progressBar()
        }

        stage.addActor(rootTable)
        super.initialize()
    }

    override fun render(delta: Float) {
        super.render(delta)
        progressBar.value = assetManager.progress * 100
    }

    override fun dispose() {
        super.dispose()
        stage.dispose()
    }
}