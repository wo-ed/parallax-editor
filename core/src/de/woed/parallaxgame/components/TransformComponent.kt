package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Entity
import ktx.ashley.mapperFor

data class TransformComponent(
        var x: Float = 0f,
        var y: Float = 0f,
        var z: Float = 0f,
        var rotation: Float = 0f
) : PooledComponent {

    override fun reset() {
        x = 0f
        y = 0f
        z = 0f
        rotation = 0f
    }
}

val transformMapper = mapperFor<TransformComponent>()
val Entity.transform: TransformComponent? get() = transformMapper[this]