package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool

interface PooledComponent : Component, Pool.Poolable