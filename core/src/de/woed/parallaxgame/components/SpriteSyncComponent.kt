package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Entity
import ktx.ashley.mapperFor

data class SpriteSyncComponent(
        var syncX: Boolean = true,
        var offsetX: Float = 0f,
        var syncY: Boolean = true,
        var offsetY: Float = 0f,
        var syncRotation: Boolean = true,
        var offsetRotation: Float = 0f
) : PooledComponent {

    override fun reset() {
        syncX = true
        offsetX = 0f
        syncY = true
        offsetY = 0f
        syncRotation = true
        offsetRotation = 0f
    }
}

val spriteSyncMapper = mapperFor<SpriteSyncComponent>()
val Entity.spriteSyncComponent: SpriteSyncComponent? get() = spriteSyncMapper[this]