package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import ktx.ashley.mapperFor

data class ParticleEffectComponent(
        var effect: ParticleEffect? = null
) : PooledComponent {

    override fun reset() {
        effect = null
    }
}

val effectMapper = mapperFor<ParticleEffectComponent>()
var Entity.effect: ParticleEffect?
    get() = effectMapper[this]?.effect
    set(value) {
        effectMapper[this]?.effect = value
    }