package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion
import ktx.ashley.mapperFor

data class AnimationComponent(
        var animation: Animation<out TextureRegion>? = null
) : PooledComponent {

    override fun reset() {
        animation = null
    }
}

val animationMapper = mapperFor<AnimationComponent>()
var Entity.animation: Animation<out TextureRegion>?
    get() = animationMapper[this]?.animation
    set(value) {
        animationMapper[this]?.animation = value
    }