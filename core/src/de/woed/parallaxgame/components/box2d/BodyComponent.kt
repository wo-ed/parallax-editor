package de.woed.parallaxgame.components.box2d

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.physics.box2d.Body
import de.woed.parallaxgame.components.PooledComponent
import ktx.ashley.mapperFor
import ktx.ashley.remove

data class BodyComponent(
        var body: Body? = null,
        var autoDestroy: Boolean = true
) : PooledComponent {

    fun destroyBody() {
        body?.let { it.world.destroyBody(it) }
        body = null
    }

    override fun reset() {
        if (autoDestroy) {
            destroyBody()
        }
        body = null
        autoDestroy = true
    }
}

val bodyMapper = mapperFor<BodyComponent>()
val Entity.bodyComponent: BodyComponent? get() = bodyMapper[this]

var Entity.body: Body?
    get() = bodyMapper[this]?.body
    set(value) {
        bodyMapper[this]?.body = value
    }

fun Entity.removeBodyComponent() {
    bodyComponent?.destroyBody()
    remove<BodyComponent>()
}