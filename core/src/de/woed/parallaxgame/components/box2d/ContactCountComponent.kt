package de.woed.parallaxgame.components.box2d

import com.badlogic.ashley.core.Entity
import de.woed.parallaxgame.components.PooledComponent
import ktx.ashley.mapperFor

data class ContactCountComponent(
        var contactCount: Int = 0
) : PooledComponent {

    override fun reset() {
        contactCount = 0
    }
}

val contactCountMapper = mapperFor<ContactCountComponent>()
val Entity.contactCountComponent: ContactCountComponent? get() = contactCountMapper[this]