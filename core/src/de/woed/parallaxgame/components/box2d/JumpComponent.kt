package de.woed.parallaxgame.components.box2d

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.Vector2
import de.woed.parallaxgame.components.PooledComponent
import ktx.ashley.mapperFor

data class JumpComponent(
        var jumpTimeOut: Float = 0f,
        val impulse: Vector2 = Vector2()
) : PooledComponent {

    override fun reset() {
        jumpTimeOut = 0f
        impulse.setZero()
    }
}

val jumpMapper = mapperFor<JumpComponent>()
val Entity.jumpComponent: JumpComponent? get() = jumpMapper[this]