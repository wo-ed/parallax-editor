package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.Matrix4
import ktx.ashley.mapperFor

data class ProjectionComponent(
        val projection: Matrix4 = Matrix4()
) : PooledComponent {
    override fun reset() {
        projection.idt()
    }
}

val projectionMapper = mapperFor<ProjectionComponent>()
val Entity.projection get() = projectionMapper[this]?.projection