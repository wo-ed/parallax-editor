package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import ktx.ashley.mapperFor

data class SpriteComponent(
        val sprite: Sprite = Sprite()
) : PooledComponent {

    override fun reset() {
        sprite.apply {
            texture = null
            setBounds(0f, 0f, 0f, 0f)
            setOrigin(0f, 0f)
            setScale(1f)
            rotation = 0f
            color = Color.WHITE
            setFlip(false, false)
        }
    }
}

val spriteMapper = mapperFor<SpriteComponent>()
val Entity.sprite: Sprite? get() = spriteMapper[this]?.sprite