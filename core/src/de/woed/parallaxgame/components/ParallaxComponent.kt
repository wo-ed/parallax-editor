package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Entity
import ktx.ashley.mapperFor

data class ParallaxComponent(
        var factorX: Float = 1f,
        var factorY: Float = 1f
) : PooledComponent {

    fun setFactor(factor: Float) {
        factorX = factor
        factorY = factor
    }

    override fun reset() {
        factorX = 1f
        factorY = 1f
    }
}

val parallaxMapper = mapperFor<ParallaxComponent>()
val Entity.parallax: ParallaxComponent? get() = parallaxMapper[this]