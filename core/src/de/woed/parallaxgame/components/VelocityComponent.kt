package de.woed.parallaxgame.components

import com.badlogic.ashley.core.Entity
import ktx.ashley.mapperFor

data class VelocityComponent(
        var x: Float = 0f,
        var y: Float = 0f
) : PooledComponent {

    override fun reset() {
        x = 0f
        y = 0f
    }
}

val velocityMapper = mapperFor<VelocityComponent>()
val Entity.velocity: VelocityComponent? get() = velocityMapper[this]