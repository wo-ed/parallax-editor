package de.woed.parallaxgame.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.SortedIteratingSystem
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.g2d.ParticleEmitter
import com.badlogic.gdx.utils.viewport.Viewport
import de.woed.parallaxgame.components.*
import de.woed.parallaxgame.inject
import de.woed.parallaxgame.utils.AscendingZComparator
import de.woed.parallaxgame.utils.render
import de.woed.parallaxgame.utils.setProjectionMatrixIfNotEqual
import ktx.ashley.oneOf

/** Draws the [SpriteComponent]s and [ParticleEffectComponent]s of its entities with respect to their [ProjectionComponent]s.
 * The entities are sorted ascending by their [TransformComponent.z].
 * If a [ParticleEmitter] does not clean up the blend function, it gets cleaned up by the system when needed.
 * After each [DrawingSystem.update], the blend function is clean.
 * [TransformComponent.z] can be used to group [ParticleEffect]s with additive or premultiplied [ParticleEmitter]s together.
 * @param viewport      Each [update] call, the [viewport] gets applied.
 * [Viewport.camera] is used to update the projection matrix of the [Batch] for entities that do not have their own [ProjectionComponent].
 * */
open class DrawingSystem(
        private val viewport: Viewport
) : SortedIteratingSystem(systemFamily, AscendingZComparator) {

    private val batch: Batch by inject()
    private val camera = viewport.camera

    private var isBlendFunctionCleanUpNeeded = false

    override fun update(deltaTime: Float) {
        viewport.apply()
        batch.render {
            super.update(deltaTime)
        }
        checkBlendFunction()
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        updateProjectionMatrix(entity)
        drawEffect(entity, deltaTime)
        drawSprite(entity)
    }

    private fun updateProjectionMatrix(entity: Entity) {
        val projection = entity.projection ?: camera.combined
        batch.setProjectionMatrixIfNotEqual(projection)
    }

    private fun drawEffect(entity: Entity, deltaTime: Float) {
        val effect = entity.effect ?: return
        drawEmitters(effect, deltaTime)
    }

    private fun drawEmitters(effect: ParticleEffect, deltaTime: Float) {
        effect.emitters.forEach { drawEmitter(it, deltaTime) }
    }

    private fun drawEmitter(emitter: ParticleEmitter, deltaTime: Float) {
        // The previous state is overridden because the emitter changes the blend function anyway.
        isBlendFunctionCleanUpNeeded = !emitter.cleansUpBlendFunction() && (emitter.isPremultipliedAlpha || emitter.isAdditive)
        emitter.draw(batch, deltaTime)
    }

    private fun drawSprite(entity: Entity) {
        val sprite = entity.sprite ?: return
        checkBlendFunction()
        sprite.draw(batch)
    }

    private fun checkBlendFunction() {
        if (isBlendFunctionCleanUpNeeded) {
            isBlendFunctionCleanUpNeeded = false
            batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
        }
    }
}

private val systemFamily = oneOf(
        SpriteComponent::class,
        ParticleEffectComponent::class
).get()