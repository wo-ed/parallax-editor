package de.woed.parallaxgame.systems.box2d

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import de.woed.parallaxgame.components.TransformComponent
import de.woed.parallaxgame.components.box2d.BodyComponent
import de.woed.parallaxgame.components.box2d.body
import de.woed.parallaxgame.components.transform
import de.woed.parallaxgame.utils.radToDeg
import ktx.ashley.allOf

open class Box2DTransformSyncSystem : IteratingSystem(systemFamily) {

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val body = entity.body ?: return
        val transform = entity.transform ?: return

        transform.x = body.position.x
        transform.y = body.position.y
        transform.rotation = radToDeg(body.angle)
    }

    companion object Instance : Box2DTransformSyncSystem()
}

private val systemFamily = allOf(
        BodyComponent::class,
        TransformComponent::class
).get()