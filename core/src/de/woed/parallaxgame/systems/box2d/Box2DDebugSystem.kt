package de.woed.parallaxgame.systems.box2d

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import de.woed.parallaxgame.inject

open class Box2DDebugSystem(
        private val world: World,
        private val camera: Camera
) : EntitySystem() {

    private val debugRenderer: Box2DDebugRenderer by inject()

    override fun update(deltaTime: Float) {
        super.update(deltaTime)
        debugRenderer.render(world, camera.combined)
    }
}