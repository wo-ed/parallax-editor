package de.woed.parallaxgame.systems.box2d

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.physics.box2d.World

open class Box2DSystem(
        private val world: World,
        private val timeStep: Float = 1 / 300f,
        private val velocityIterations: Int = 6,
        private val positionIterations: Int = 2,
        private val maxFrameTime: Float = 0.25f
) : EntitySystem() {

    private var accumulator = 0f

    override fun update(deltaTime: Float) {
        super.update(deltaTime)
        doPhysicsStep(deltaTime)
    }

    // Fixed time step
    private fun doPhysicsStep(deltaTime: Float) {

        // Max frame time to avoid spiral of death (on slow devices)
        val frameTime = Math.min(deltaTime, maxFrameTime)

        accumulator += frameTime
        while (accumulator >= timeStep) {
            world.step(timeStep, velocityIterations, positionIterations)
            accumulator -= timeStep
        }
    }
}