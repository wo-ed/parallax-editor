package de.woed.parallaxgame.systems.box2d

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import de.woed.parallaxgame.components.box2d.*
import ktx.ashley.allOf

open class JumpSystem : IteratingSystem(systemFamily) {

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val body = entity.body ?: return
        val jumpComponent = entity.jumpComponent ?: return

        jumpComponent.jumpTimeOut--

        if (canJump(entity) && (Gdx.input.isKeyPressed(Input.Keys.SPACE) || Gdx.input.isTouched)) {
            body.applyLinearImpulse(jumpComponent.impulse, body.localCenter, true)
            jumpComponent.jumpTimeOut += 15f
        }
    }

    private fun canJump(entity: Entity): Boolean {
        val contactCountComponent = entity.contactCountComponent ?: return false
        val jumpComponent = entity.jumpComponent ?: return false
        return contactCountComponent.contactCount >= 1 && jumpComponent.jumpTimeOut <= 0
    }

    companion object Instance : JumpSystem()
}

private val systemFamily = allOf(
        JumpComponent::class,
        ContactCountComponent::class,
        BodyComponent::class
).get()