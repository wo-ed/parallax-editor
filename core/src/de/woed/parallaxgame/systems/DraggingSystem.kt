package de.woed.parallaxgame.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.SortedIteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.Viewport
import de.woed.parallaxgame.ParallaxCamera
import de.woed.parallaxgame.components.*
import de.woed.parallaxgame.utils.DescendingZComparator
import ktx.ashley.allOf
import ktx.ashley.oneOf

open class DraggingSystem(viewport: Viewport) : SortedIteratingSystem(systemFamily, DescendingZComparator) {
    private val camera = viewport.camera

    private val mousePosition = Vector3()
    private val oldMousePosition = Vector3()
    private val deltaMousePosition = Vector3()

    var dragChanged: (() -> Unit)? = null
    var isEntitySelected: ((Entity) -> Boolean)? = null
    var isTouched: (() -> Boolean)? = null

    override fun update(deltaTime: Float) {
        mousePosition.set(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f)
        deltaMousePosition.set(mousePosition).sub(oldMousePosition)
        super.update(deltaTime)
        oldMousePosition.set(mousePosition)
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val transform = entity.transform ?: return
        val sprite = entity.sprite ?: return

        val camera = camera as ParallaxCamera
        val zoom = camera.zoom
        if (isEntitySelected?.invoke(entity) == true && isTouched?.invoke() == true) {
            transform.x += deltaMousePosition.x * zoom
            transform.y -= deltaMousePosition.y * zoom

            /** TODO Find a different way of doing this.
             * (This way, the color of an entity cannot be specified individually.
             * Also, a texture with a dark color or the same color as [draggingColor] ist not affected by the tint.) */
            sprite.color = draggingColor
        } else {
            sprite.color = regularColor
        }
    }

    companion object {
        private val regularColor = Color.WHITE
        private val draggingColor = Color.BLUE
    }
}

private val systemFamily = allOf(
        DraggableComponent::class,
        TransformComponent::class
).oneOf(
        SpriteComponent::class,
        ParticleEffectComponent::class
).get()
