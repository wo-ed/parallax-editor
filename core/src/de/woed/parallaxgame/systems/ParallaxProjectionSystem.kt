package de.woed.parallaxgame.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import de.woed.parallaxgame.ParallaxCamera
import de.woed.parallaxgame.components.ParallaxComponent
import de.woed.parallaxgame.components.ProjectionComponent
import de.woed.parallaxgame.components.parallax
import de.woed.parallaxgame.components.projection
import ktx.ashley.allOf

class ParallaxProjectionSystem(private val camera: ParallaxCamera) : IteratingSystem(systemFamily) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val parallax = entity.parallax
        val x = parallax?.factorX ?: 1f
        val y = parallax?.factorY ?: 1f
        val projection = camera.calculateParallaxMatrix(x, y)
        entity.projection?.set(projection)
    }
}

private val systemFamily = allOf(
        ParallaxComponent::class,
        ProjectionComponent::class
).get()