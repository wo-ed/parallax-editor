package de.woed.parallaxgame.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import de.woed.parallaxgame.components.TransformComponent
import de.woed.parallaxgame.components.VelocityComponent
import de.woed.parallaxgame.components.transform
import de.woed.parallaxgame.components.velocity
import ktx.ashley.allOf

open class VelocitySystem : IteratingSystem(systemFamily) {

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val position = entity.transform ?: return
        val velocity = entity.velocity ?: return

        position.x += velocity.x * deltaTime
        position.y += velocity.y * deltaTime
    }

    companion object Instance : VelocitySystem()
}

private val systemFamily = allOf(
        TransformComponent::class,
        VelocityComponent::class
).get()