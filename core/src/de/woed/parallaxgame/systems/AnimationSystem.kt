package de.woed.parallaxgame.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import de.woed.parallaxgame.components.AnimationComponent
import de.woed.parallaxgame.components.SpriteComponent
import de.woed.parallaxgame.components.animation
import de.woed.parallaxgame.components.sprite
import ktx.ashley.allOf

open class AnimationSystem : IteratingSystem(systemFamily) {

    private var stateTime = 0f

    override fun update(deltaTime: Float) {
        super.update(deltaTime)
        stateTime += deltaTime
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val sprite = entity.sprite ?: return
        val animation = entity.animation ?: return
        val region = animation.getKeyFrame(stateTime)
        sprite.setRegion(region)
    }

    companion object Instance : AnimationSystem()
}

private val systemFamily = allOf(
        AnimationComponent::class,
        SpriteComponent::class
).get()