package de.woed.parallaxgame.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import de.woed.parallaxgame.components.*
import ktx.ashley.allOf

open class SpriteSyncSystem : IteratingSystem(systemFamily) {

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val (x, y, _, rotation) = entity.transform ?: return
        val sprite = entity.sprite ?: return
        val syncComponent = entity.spriteSyncComponent ?: return

        if (syncComponent.syncX) {
            sprite.x = x + syncComponent.offsetX
        }

        if (syncComponent.syncY) {
            sprite.y = y + syncComponent.offsetY
        }

        if (syncComponent.syncRotation) {
            sprite.rotation = rotation + syncComponent.offsetRotation
        }
    }

    companion object Instance : SpriteSyncSystem()
}

private val systemFamily = allOf(
        TransformComponent::class,
        SpriteComponent::class,
        SpriteSyncComponent::class
).get()