package de.woed.parallaxgame.demo

import box2dLight.PointLight
import box2dLight.RayHandler
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.utils.viewport.ExtendViewport
import de.woed.parallaxgame.inject
import de.woed.parallaxgame.screens.BaseScreen
import de.woed.parallaxgame.utils.color
import de.woed.parallaxgame.utils.pointLight
import ktx.box2d.createWorld
import ktx.box2d.earthGravity

class SunScreen : BaseScreen() {
    private val world = createWorld(earthGravity, allowSleep = true)
    private val rayHandler: RayHandler by inject()
    private val camera = OrthographicCamera()
    override val viewport = ExtendViewport(16f, 9f, camera)

    private lateinit var sun: PointLight

    init {
        rayHandler.setAmbientLight(color(r = 64))
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewport.update(width, height, false)
    }

    override fun show() {
        super.show()
        rayHandler.setWorld(world)
        sun = rayHandler.pointLight(128, color(253, 184, 19))
    }

    override fun render(delta: Float) {
        super.render(delta)
        rayHandler.setCombinedMatrix(camera)
        rayHandler.updateAndRender()
    }

    override fun hide() {
        super.hide()
        rayHandler.setWorld(null)
        sun.remove(true)
    }

    override fun dispose() {
        super.dispose()
        world.dispose()
    }
}