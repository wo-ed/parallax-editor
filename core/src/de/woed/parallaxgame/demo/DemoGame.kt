package de.woed.parallaxgame.demo

import box2dLight.RayHandler
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.kotcrab.vis.ui.VisUI
import de.woed.parallaxgame.BaseGame
import de.woed.parallaxgame.ContextProvider
import de.woed.parallaxgame.OsManager
import de.woed.parallaxgame.screens.BaseScreen
import de.woed.parallaxgame.screens.LoadingScreen
import ktx.inject.Context

class DemoGame(
        private val osManager: OsManager
) : BaseGame(), ContextProvider {

    override val context by lazy { Context() }
    private val inputMultiplexer by lazy { InputMultiplexer() }
    override val assetManager by lazy { AssetManager() }

    override var bgColor: Color = Color.BLACK

    override fun create() {
        VisUI.load()
        registerProviders()
        Gdx.input.inputProcessor = inputMultiplexer
        loadScreen(TomatoScreen())
    }

    private fun registerProviders() = context.register {
        bindSingleton(this@DemoGame)
        bindSingleton(osManager)
        bindSingleton(inputMultiplexer)
        bindSingleton(assetManager)
        bindSingleton<Batch>(SpriteBatch())
        bindSingleton(Box2DDebugRenderer())
        bindSingleton(ShapeRenderer())
        bindSingleton(RayHandler(null))
    }

    override fun checkNextScreen() {
        if (nextScreen?.isLoaded() == true) {
            screen?.dispose()
            super.showNextScreen()
        }
    }

    override fun loadScreen(screen: BaseScreen) {
        this.screen?.dispose()
        super.loadScreen(screen)
        LoadingScreen().apply {
            initialize()
            setScreen(this)
        }
    }

    override fun dispose() {
        super.dispose()
        screen?.dispose()
        nextScreen?.dispose()
        context.dispose()
        VisUI.dispose()
    }
}