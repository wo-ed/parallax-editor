package de.woed.parallaxgame.demo

import com.badlogic.gdx.assets.loaders.ParticleEffectLoader
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP_PINGPONG
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP_RANDOM
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import de.woed.parallaxgame.utils.AtlasAnimationDescriptor
import de.woed.parallaxgame.utils.AtlasRegionDescriptor
import ktx.assets.assetDescriptor

// Folders
const val ATLASES = "atlases"
const val EFFECTS = "effects"

// Asset descriptors
val actorAtlasDesc = assetDescriptor<TextureAtlas>("$ATLASES/actors.atlas")
val demoAtlasDesc = assetDescriptor<TextureAtlas>("$ATLASES/demo.atlas")
val tomatoBiteDesc = AtlasAnimationDescriptor("tomato/bite", actorAtlasDesc, 0.125f, LOOP_PINGPONG)
val pumpkinFlashingDesc = AtlasAnimationDescriptor("pumpkin/flashing", actorAtlasDesc, 0.125f, LOOP_RANDOM)
val skullFlashingDesc = AtlasAnimationDescriptor("skull/flashing", actorAtlasDesc, 0.125f, LOOP_RANDOM)
val tileDesc = AtlasRegionDescriptor("tile", demoAtlasDesc)
val thicketDesc = AtlasRegionDescriptor("thicket", demoAtlasDesc)
val boxDesc = AtlasRegionDescriptor("box", demoAtlasDesc)
val ghostDesc = assetDescriptor<ParticleEffect>("$EFFECTS/ghost.p", ParticleEffectLoader.ParticleEffectParameter().apply {
    atlasFile = demoAtlasDesc.fileName
})