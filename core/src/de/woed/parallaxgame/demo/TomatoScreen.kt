package de.woed.parallaxgame.demo

import box2dLight.PointLight
import box2dLight.RayHandler
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.utils.viewport.ExtendViewport
import de.woed.parallaxgame.ParallaxCamera
import de.woed.parallaxgame.components.*
import de.woed.parallaxgame.components.box2d.BodyComponent
import de.woed.parallaxgame.components.box2d.ContactCountComponent
import de.woed.parallaxgame.components.box2d.JumpComponent
import de.woed.parallaxgame.components.box2d.body
import de.woed.parallaxgame.inject
import de.woed.parallaxgame.screens.BaseScreen
import de.woed.parallaxgame.systems.AnimationSystem
import de.woed.parallaxgame.systems.DrawingSystem
import de.woed.parallaxgame.systems.ParallaxProjectionSystem
import de.woed.parallaxgame.systems.SpriteSyncSystem
import de.woed.parallaxgame.systems.box2d.Box2DDebugSystem
import de.woed.parallaxgame.systems.box2d.Box2DSystem
import de.woed.parallaxgame.systems.box2d.Box2DTransformSyncSystem
import de.woed.parallaxgame.systems.box2d.JumpSystem
import de.woed.parallaxgame.utils.*
import ktx.ashley.create
import ktx.ashley.entity
import ktx.box2d.body
import ktx.box2d.createWorld
import ktx.box2d.distanceJointWith
import ktx.box2d.earthGravity

class TomatoScreen : BaseScreen() {
    private val camera = ParallaxCamera()
    override val viewport = ExtendViewport(16f / 2f, 9f / 2f, camera)

    override val assetDescriptors = Companion.assetDescriptors

    private val world = createWorld(earthGravity, allowSleep = true).apply {
        setContactListener(CountingContactAdapter)
    }

    private lateinit var tomato: Entity
    private lateinit var pumpkin: Entity
    private lateinit var skull: Entity

    private val engine = PooledEngine().apply {
        /** TODO Keep the [BodyDestroyer] or the reset() method inside [BodyComponent]?
        val bodyFamily = allOf(BodyComponent::class).get()
        addEntityListener(bodyFamily, BodyDestroyer) */
    }

    private val rayHandler: RayHandler by inject()
    private lateinit var tomatoLight: PointLight
    private lateinit var pumpkinLight: PointLight
    private lateinit var skullLight: PointLight

    override fun initialize() {
        camera.position.y -= 2
        initSystems()
        initEntities()
        super.initialize()
    }

    private fun initSystems() {
        engine.addSystems(
                JumpSystem,
                Box2DSystem(world),
                Box2DTransformSyncSystem,
                SpriteSyncSystem,
                AnimationSystem,
                ParallaxProjectionSystem(camera),
                DrawingSystem(viewport)
        )

        if (IS_DRAW_DEBUG_ENABLED) {
            engine += Box2DDebugSystem(world, camera)
        }
    }

    private fun initEntities() {
        initPlayerEntity()
        initGroundEntity()
        initEnemyEntities()
        initSurroundings()
    }

    private fun initPlayerEntity() {
        val animation = assetManager[tomatoBiteDesc]
        val region = animation.keyFrames.first()
        val textureWidth = region.regionWidth.toFloat()
        val textureHeight = region.regionHeight.toFloat()

        val x = 0f
        val y = 3f
        val boxWidth = 0.3f
        val boxHeight = 0.3f

        val spriteWidth: Float = boxWidth
        val spriteHeight: Float = boxHeight

        val originX = 64f * spriteWidth / textureWidth
        val originY = 50f * spriteHeight / textureHeight

        tomato = engine.entity {
            with<ContactCountComponent> {}
            with<TransformComponent> {
                z = 1f
            }
            with<AnimationComponent> {
                this.animation = animation
            }
            with<JumpComponent> {
                impulse.set(0f, 6f)
            }
            with<SpriteComponent> {
                sprite.setSize(spriteWidth, spriteHeight)
                sprite.setScale(2f)
                sprite.setOrigin(originX, originY)
            }
            with<SpriteSyncComponent> {
                offsetX = -originX
                offsetY = -originY
                syncRotation = false
            }
//            with<ParticleEffectComponent> {
//                effect = assetManager[ghostDesc]
//                effect?.scaleEffect(0.01f)
//            }
        }

        val body = world.body {
            type = BodyDef.BodyType.DynamicBody
            position.set(x, y)
            box(boxWidth, boxHeight)
            box(boxWidth * 0.75f, boxHeight / 8f, Vector2(0f, -boxHeight / 2f)) {
                isSensor = true
                userData = tomato
            }
        }

        tomato += engine.create<BodyComponent> {
            this.body = body
        }
    }

    private fun initGroundEntity() {
        val anchor = world.body {
            box(1f, 1f, Vector2(0f, 10f)) {
                isSensor = true
            }
        }

        engine.boxSpriteEntity(world, boxWidth = 9f, boxHeight = 0.6f, region = assetManager[tileDesc]).apply {
            body?.apply {
                type = BodyDef.BodyType.DynamicBody
                gravityScale = 0f
                distanceJointWith(anchor) {
                    frequencyHz = 0.5f
                    dampingRatio = 0.5f
                    length = 0.5f
                }
            }
        }
    }

    private fun initEnemyEntities() {
        initPumpkin()
        initSkull()
    }

    private fun initPumpkin() {
        val animation = assetManager[pumpkinFlashingDesc]
        val region = animation.keyFrames.first()

        pumpkin = engine.boxSpriteEntity(world, 0f, 2f, 0.6f, 0.6f, region).apply {
            body?.type = BodyDef.BodyType.DynamicBody
            sprite?.apply {
                setScale(2f)
                setOrigin(64f * width / region.regionWidth, 47f * height / region.regionHeight)
            }
            this += engine.create<AnimationComponent> {
                this.animation = animation
            }
            spriteSyncComponent?.offsetX = sprite?.originX?.unaryMinus() ?: 0f
            spriteSyncComponent?.offsetY = sprite?.originY?.unaryMinus() ?: 0f
            transform?.z = 0.5f
        }
    }

    private fun initSkull() {
        val animation = assetManager[skullFlashingDesc]

        skull = engine.boxSpriteEntity(world, 0f, 2f, 0.9f, 0.9f, animation.keyFrames.first()).apply {
            body?.type = BodyDef.BodyType.DynamicBody
            sprite?.setScale(2f)
            this += engine.create<AnimationComponent> {
                this.animation = animation
            }
        }
    }

    private fun initSurroundings() {
        val region = assetManager[thicketDesc]

        val width = 8f
        val count = MathUtils.ceil(viewport.worldWidth / width) + 1

        // This approach is not working if the viewport changes.
        for (i in 0 until count) {
            engine.parallaxEntity(region, parallaxX = 4f).apply {
                transform?.apply {
                    x = camera.position.x - viewport.worldWidth / 2f + (i - 1) * width
                    y = camera.position.y - viewport.worldHeight / 2f - 1
                    z = 10f
                }
                sprite?.setWidth(width, keepAspectRatio = true)
            }
        }
    }

    override fun render(delta: Float) {
        super.render(delta)

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            pitch(-9f, delta)
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            pitch(9f, delta)
        }

        val pitch = Gdx.input.pitch
        if (pitch != 0f) {
            pitch(pitch, delta)
        }

        tomato.body?.position?.let { tomatoLight.setPosition(it.x, it.y) }
        pumpkin.body?.position?.let { pumpkinLight.setPosition(it.x, it.y) }
        skull.body?.position?.let { skullLight.setPosition(it.x, it.y) }

        rayHandler.setCombinedMatrix(camera)
        rayHandler.updateAndRender()
        engine.update(delta)
    }

    private fun pitch(pitch: Float, delta: Float) {
        val playerBody = tomato.body ?: return
        val rollVelocity = tmpV.set(world.gravity.y * MathUtils.sinDeg(pitch), 0f)
        val deltaPosition = rollVelocity.scl(delta)
        playerBody.setTransform(playerBody.transform.position.add(deltaPosition), playerBody.angle)
        val radians = Math.asin(rollVelocity.x / world.gravity.y.toDouble() * 30f)
        tomato.sprite!!.rotation += radToDeg(radians.toFloat())
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewport.update(width, height, false)
    }

    override fun show() {
        super.show()
        rayHandler.setWorld(world)
        tomatoLight = rayHandler.pointLight(128, color = Color.RED, distance = 3f)
        pumpkinLight = rayHandler.pointLight(128, color = Color.ORANGE, distance = 3f)
        skullLight = rayHandler.pointLight(128, color = Color.BLUE, distance = 3f)
    }

    override fun hide() {
        super.hide()
        rayHandler.setWorld(null)
        tomatoLight.remove(true)
        pumpkinLight.remove(true)
        skullLight.remove(true)
    }

    override fun dispose() {
        super.dispose()
        world.dispose()
    }

    companion object {
        private const val IS_DRAW_DEBUG_ENABLED = false

        private val tmpV = Vector2()

        private val assetDescriptors = tomatoBiteDesc.dependencies +
                pumpkinFlashingDesc.dependencies +
                skullFlashingDesc.dependencies +
                tileDesc.dependencies +
                thicketDesc.dependencies +
                ghostDesc
    }
}