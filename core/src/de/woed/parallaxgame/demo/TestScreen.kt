package de.woed.parallaxgame.demo

import box2dLight.PointLight
import box2dLight.RayHandler
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.utils.viewport.ExtendViewport
import de.woed.parallaxgame.components.TransformComponent
import de.woed.parallaxgame.components.box2d.BodyComponent
import de.woed.parallaxgame.inject
import de.woed.parallaxgame.screens.BaseScreen
import de.woed.parallaxgame.systems.box2d.Box2DDebugSystem
import de.woed.parallaxgame.systems.box2d.Box2DSystem
import de.woed.parallaxgame.systems.box2d.Box2DTransformSyncSystem
import de.woed.parallaxgame.utils.addSystems
import de.woed.parallaxgame.utils.pointLight
import ktx.ashley.entity
import ktx.box2d.body
import ktx.box2d.createWorld
import ktx.box2d.earthGravity

class TestScreen : BaseScreen() {
    private val world = createWorld(earthGravity, allowSleep = true)
    private val rayHandler: RayHandler by inject()
    private val camera = OrthographicCamera()
    override val viewport = ExtendViewport(16f, 9f, camera)
    private val engine = PooledEngine()

    private lateinit var sun: PointLight

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewport.update(width, height, false)
    }

    override fun show() {
        super.show()
        rayHandler.setWorld(world)
        sun = rayHandler.pointLight(128, Color.PURPLE)
    }

    override fun initialize() {
        engine.entity {
            with<TransformComponent> {}
            with<BodyComponent> {
                body = world.body {
                    gravityScale = 0.01f
                    type = BodyDef.BodyType.DynamicBody
                    polygon(floatArrayOf(0f, 2f, -3f, -2f, 3f, -2f))
                    position.set(0f, 4f)
                }
            }
        }

        engine.addSystems(
                Box2DSystem(world),
                Box2DTransformSyncSystem,
                Box2DDebugSystem(world, camera)
        )

        super.initialize()
    }

    override fun render(delta: Float) {
        super.render(delta)
        rayHandler.setCombinedMatrix(camera)
        rayHandler.updateAndRender()
        engine.update(delta)
    }

    override fun hide() {
        super.hide()
        rayHandler.setWorld(null)
        sun.remove(true)
    }

    override fun dispose() {
        super.dispose()
        world.dispose()
    }
}