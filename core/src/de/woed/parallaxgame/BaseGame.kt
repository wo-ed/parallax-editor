package de.woed.parallaxgame

import com.badlogic.gdx.Game
import com.badlogic.gdx.assets.AssetManager
import de.woed.parallaxgame.screens.BaseScreen
import de.woed.parallaxgame.utils.Clearable

abstract class BaseGame : Game(), Clearable {
    protected abstract val assetManager: AssetManager

    protected var nextScreen: BaseScreen? = null

    override fun render() {
        clearColor()
        assetManager.update()
        checkNextScreen()
        super.render()
    }

    protected open fun checkNextScreen() {
        if (nextScreen?.isLoaded() == true) {
            showNextScreen()
        }
    }

    protected fun showNextScreen() {
        setScreen(nextScreen)
        nextScreen?.initialize()
        nextScreen = null
    }

    open fun loadScreen(screen: BaseScreen) {
        screen.load()
        nextScreen = screen
    }
}