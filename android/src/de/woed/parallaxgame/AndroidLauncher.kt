package de.woed.parallaxgame

import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import de.woed.parallaxgame.demo.DemoGame

class AndroidLauncher : AndroidApplication() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val osManager = AndroidOsManager()
        val myGdxGame = DemoGame(osManager)
        val config = AndroidApplicationConfiguration()
        initialize(myGdxGame, config)
    }
}