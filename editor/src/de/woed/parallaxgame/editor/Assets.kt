package de.woed.parallaxgame.editor

import com.badlogic.gdx.graphics.Texture
import ktx.assets.assetDescriptor

// Folders
const val TEXTURES = "textures"
const val EXPORTS = "exports"
const val EXPORTED_MAPS = "$EXPORTS/maps"

// Textures
val pixelDesc = assetDescriptor<Texture>("$TEXTURES/pixel.png")