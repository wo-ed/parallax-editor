package de.woed.parallaxgame.editor

import com.badlogic.gdx.graphics.Color

val supportedTextureFormats = listOf("png", "jpg", "bmp")
fun isTextureFormatSupported(extension: String) = supportedTextureFormats.contains(extension.toLowerCase())

val supportedAtlasFormats = listOf("atlas")
fun isTextureAtlasFormatSupported(extension: String) = supportedAtlasFormats.contains(extension.toLowerCase())

val supportedEffectFormats = listOf("p")
fun isEffectFormatSupported(extension: String) = supportedEffectFormats.contains(extension.toLowerCase())

val supportedAssetFormats = supportedTextureFormats + supportedAtlasFormats + supportedEffectFormats
fun isAssetFormatSupported(extension: String) = supportedAssetFormats.contains(extension.toLowerCase())

val editorBackground = Color(64 / 255f, 64 / 255f, 64 / 255f, 1f)
val editorBackgroundDark = Color(51 / 255f, 51 / 255f, 51 / 255f, 1f)