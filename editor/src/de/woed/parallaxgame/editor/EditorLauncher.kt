package de.woed.parallaxgame.editor

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.backends.lwjgl.LwjglCanvas
import de.woed.parallaxgame.utils.toAwtColor
import java.awt.Canvas
import java.awt.Toolkit
import java.awt.datatransfer.DataFlavor
import java.awt.dnd.*
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.io.File
import javax.swing.JFrame
import javax.swing.SwingUtilities

object EditorLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val screenSize = Toolkit.getDefaultToolkit().screenSize
        val editorGame = EditorGame()

        val config = LwjglApplicationConfiguration().apply {
            width = screenSize.width
            height = screenSize.height
        }

        val lwjglCanvas = LwjglCanvas(editorGame, config)
        val canvas = lwjglCanvas.canvas

        SwingUtilities.invokeLater {
            val jFrame = jFrame(config.width, config.height, canvas)
            val screen = EditorScreen()
            editorGame.loadScreen(screen)

            val dropTargetListener = MyDropTargetListener(editorGame, screen)
            DropTarget(jFrame, DnDConstants.ACTION_COPY_OR_MOVE, dropTargetListener, true)
        }
    }
}

private fun jFrame(width: Int, height: Int, canvas: Canvas): JFrame {
    val bgColor = editorBackground.toAwtColor()

    return JFrame("Editor").apply {
        setSize(width, height)
        background = bgColor
        contentPane.background = bgColor
        canvas.size = size
        add(canvas)
        pack()

        addWindowListener(object : WindowAdapter() {
            override fun windowClosing(event: WindowEvent) {
                Gdx.app.exit()
            }
        })

        setLocationRelativeTo(null)
        defaultCloseOperation = JFrame.HIDE_ON_CLOSE
        isVisible = true
    }
}

private class MyDropTargetListener(
        private val game: Game,
        private val screen: EditorScreen
) : DropTargetListener {

    override fun drop(dtde: DropTargetDropEvent) {
        if (game.screen !== screen || !screen.isLoaded()) {
            dtde.rejectDrop()
            return
        }

        dtde.acceptDrop(DnDConstants.ACTION_COPY)
        val transferData = dtde.transferable.getTransferData(DataFlavor.javaFileListFlavor)
        val files = (transferData as List<*>).filterIsInstance<File>()
        files.forEach { screen.onDrop(it.path) }
    }

    override fun dragOver(dtde: DropTargetDragEvent) {}
    override fun dragExit(dte: DropTargetEvent) {}
    override fun dropActionChanged(dtde: DropTargetDragEvent) {}
    override fun dragEnter(dtde: DropTargetDragEvent) {}
}