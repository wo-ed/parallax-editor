package de.woed.parallaxgame.editor

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.GdxRuntimeException
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.kotcrab.vis.ui.FocusManager
import com.kotcrab.vis.ui.widget.VisCheckBox
import com.kotcrab.vis.ui.widget.VisTable
import com.kotcrab.vis.ui.widget.VisValidatableTextField
import de.woed.parallaxgame.ParallaxCamera
import de.woed.parallaxgame.components.*
import de.woed.parallaxgame.inject
import de.woed.parallaxgame.screens.StageBaseScreen
import de.woed.parallaxgame.systems.DraggingSystem
import de.woed.parallaxgame.systems.DrawingSystem
import de.woed.parallaxgame.systems.ParallaxProjectionSystem
import de.woed.parallaxgame.systems.SpriteSyncSystem
import de.woed.parallaxgame.utils.*
import ktx.actors.KtxInputListener
import ktx.actors.onChange
import ktx.actors.onClick
import ktx.ashley.create
import ktx.ashley.entity
import ktx.log.error
import ktx.vis.menu
import ktx.vis.menuBar
import ktx.vis.menuItem
import ktx.vis.table
import org.lwjgl.input.Mouse
import org.redundent.kotlin.xml.xml

class EditorScreen : StageBaseScreen() {
    private val batch: Batch by inject()

    override val assetDescriptors = Companion.assetDescriptors
    override val stage = Stage(ScreenViewport(), batch)
    private val engine = PooledEngine()
    private val camera = ParallaxCamera()
    override val viewport = ScreenViewport(camera)
    private val inputAdapter = InputAdapter()

    private var entityRect = Rectangle()

    private var selectedEntity: Entity? = null
    private var dndAssets = setOf<AssetDescriptor<*>>()
    private var loadingDndAssets = listOf<AssetDescriptor<*>>()

    private val pixel by lazy { assetManager[pixelDesc] }

    private lateinit var leftTable: VisTable
    private lateinit var contentTable: VisTable
    private lateinit var xField: VisValidatableTextField
    private lateinit var yField: VisValidatableTextField
    private lateinit var zField: VisValidatableTextField
    private lateinit var parallaxCheckbox: VisCheckBox
    private lateinit var zParallaxButton: VisCheckBox
    private lateinit var customParallaxButton: VisCheckBox
    private lateinit var xParallaxField: VisValidatableTextField
    private lateinit var yParallaxField: VisValidatableTextField
    private lateinit var rotationField: VisValidatableTextField

    private val nextZIndex
        get() = engine.entities.mapNotNull { it.transform?.z }.max()?.plus(1) ?: 0f

    private var isContentTouched = false
    private var isMouseOverContent = false

    private val drawingSystem = DrawingSystem(viewport)
    private val draggingSystem = DraggingSystem(viewport).apply {
        isEntitySelected = { selectedEntity === it }
        isTouched = this@EditorScreen::isContentTouched
        dragChanged = {
            if (selectedEntity != null) {
                resetFocus()
            }
        }
    }

    init {
        bgColor = editorBackground
    }

    override fun initialize() {
        initSystems()
        initStage()
        super.initialize()
    }

    private fun initSystems() = engine.addSystems(
            draggingSystem,
            SpriteSyncSystem,
            ParallaxProjectionSystem(camera),
            drawingSystem
    )

    private fun initStage() {
        val menuBar = menuBar {
            menu("File") {
                menuItem("New")
                menuItem("Open")
                menuItem("Save") {
                    onClick { save() }
                }
            }
            menu("Edit") {
                menuItem("Undo") {
                    setShortcut(Input.Keys.CONTROL_LEFT, Input.Keys.Z)
                }
                menuItem("Undo")
            }
        }

        val rootTable = table {
            setFillParent(true)

            // Header (menu bar)
            add(menuBar.table).top().colspan(32).growX().row()

            // Left content
            leftTable = table { leftTableCell ->
                defaults().pad(8f)
                leftTableCell.colspan(1).grow()
                background = TextureRegionDrawable(TextureRegion(pixel))
                color = editorBackgroundDark
                xField = floatField { it.growX().row() }
                yField = floatField { it.growX().row() }
                zField = floatField { it.growX().row() }
                rotationField = floatField { it.growX().row() }
                addSeparator()
                parallaxCheckbox = checkBox("Parallax scrolling") {
                    left()
                    it.growX().row()
                }
                buttonTable { buttonTableCell ->
                    zParallaxButton = radioButton("Use z-position as parallax values") {
                        left()
                        it.growX().row()
                    }
                    customParallaxButton = radioButton("Custom parallax values") {
                        left()
                        it.growX().row()
                    }
                    buttonTableCell.growX().row()
                }
                xParallaxField = floatField(1f) { it.growX().row() }
                yParallaxField = floatField(1f) { it.growX().row() }
            }

            // Right content
            contentTable = table {
                it.colspan(31).grow()
            }
        }

        stage.addActor(rootTable)

        xField.onChange { selectedEntity?.transform?.x = xField.text.toFloatOrNull() ?: return@onChange }
        yField.onChange { selectedEntity?.transform?.y = yField.text.toFloatOrNull() ?: return@onChange }
        zField.onChange {
            selectedEntity?.transform?.z = zField.text.toFloatOrNull() ?: return@onChange
            draggingSystem.forceSort()
            drawingSystem.forceSort()
        }
        rotationField.onChange { selectedEntity?.transform?.rotation = rotationField.text.toFloatOrNull() ?: return@onChange }

        contentTable.touchable = Touchable.enabled
        contentTable.addListener(object : KtxInputListener() {
            override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                resetFocus()
                val sortedEntities = engine.entities.sortedByDescending { it.transform?.z ?: 0f }
                selectedEntity = sortedEntities.firstOrNull { isMouseOverEntity(it) }
                isContentTouched = true
                return true
            }

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                super.touchUp(event, x, y, pointer, button)
                isContentTouched = false
            }

            override fun enter(event: InputEvent, x: Float, y: Float, pointer: Int, fromActor: Actor?) {
                super.enter(event, x, y, pointer, fromActor)
                isMouseOverContent = true
            }

            override fun exit(event: InputEvent, x: Float, y: Float, pointer: Int, toActor: Actor?) {
                super.exit(event, x, y, pointer, toActor)
                isMouseOverContent = false
            }
        })

        customParallaxButton.isVisible = false

        xParallaxField.isVisible = false
        xParallaxField.onChange { selectedEntity?.parallax?.factorX = xParallaxField.text.toFloatOrNull() ?: return@onChange }

        yParallaxField.isVisible = false
        yParallaxField.onChange { selectedEntity?.parallax?.factorY = yParallaxField.text.toFloatOrNull() ?: return@onChange }
    }

    // TODO Only visible area
    // TODO Particle effects
    private fun isMouseOverEntity(entity: Entity): Boolean {
        val sprite = entity.sprite ?: return false
        val projection = entity.projection ?: camera.combined

        val worldPosition = camera.unprojectParallax(
                Vector3(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f),
                viewport.screenX.toFloat(),
                viewport.screenY.toFloat(),
                viewport.screenWidth.toFloat(),
                viewport.screenHeight.toFloat(),
                projection
        )

        entityRect.set(sprite.boundingRectangle)
        return entityRect.contains(worldPosition.x, worldPosition.y)
    }

    override fun render(delta: Float) {
        drawBackground()
        createDndEntities()
        handleInput()
        drawBackground(viewport)
        // drawLines()
        engine.update(delta)
        drawSelection()
        updateFields()
        renderStage(delta)
    }

    private fun createDndEntities() {
        val descriptors = loadingDndAssets.takeWhile { assetManager.isLoaded(it) }
        loadingDndAssets -= descriptors
        dndAssets += descriptors

        descriptors.forEach {
            val asset = assetManager[it]
            createDndEntity(asset)
        }
    }

    private fun <T> createDndEntity(asset: T) {
        val entity = when (asset) {
            is Texture -> createTextureRegionEntity(TextureRegion(asset))
            is ParticleEffect -> {
                val entity = createEntity()
                entity += engine.create<ParticleEffectComponent> { effect = asset }
                entity
            }
            is TextureAtlas -> {
                val entities = asset.regions.map {
                    createTextureRegionEntity(it)
                }
                entities.first()
            }
            else -> null
        }

        this.selectedEntity = entity
    }

    private fun createTextureRegionEntity(region: TextureRegion): Entity {
        val entity = createEntity()

        entity += engine.create<SpriteComponent> {
            sprite.setRegion(region)
            sprite.setSize(region.regionWidth.toFloat(), region.regionHeight.toFloat())
            sprite.setOriginCenter()
        }

        entity += engine.create<SpriteSyncComponent> {}

        return entity
    }

    private fun createEntity(): Entity {
        return engine.entity {
            with<DraggableComponent> {}
            with<ParallaxComponent> {}
            with<ProjectionComponent> {}
            with<TransformComponent> {
                x = camera.position.x
                y = camera.position.y
                z = nextZIndex
            }
        }
    }

    private fun handleInput() {
        handleKeyboardInput()
        // handleMouseInput()
    }

    private fun handleKeyboardInput() {
        val velocity = 10f
        arrowKeyboardMovements.forEach { (key, dimension, sign) ->
            if (Gdx.input.isKeyPressed(key) && hasFocus(null)) {
                when (dimension) {
                    Dimension.X -> camera.position.x += sign * velocity
                    Dimension.Y -> camera.position.y += sign * velocity
                }
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.PLUS) && hasFocus(null)) {
            zoom(-DELTA_ZOOM)
        }

        if (Gdx.input.isKeyPressed(Input.Keys.MINUS) && hasFocus(null)) {
            zoom(DELTA_ZOOM)
        }
    }

    private fun zoom(deltaZoom: Float) {
        camera.zoom += deltaZoom
        camera.zoom = maxOf(camera.zoom, MIN_ZOOM)
        camera.zoom = minOf(camera.zoom, MAX_ZOOM)
    }

    private fun handleMouseInput() {
        if (!Mouse.isInsideWindow() || !isMouseOverContent) {
            return
        }

        val velocity = 5f
        val areaX = 50
        val areaY = 50
        val screenX = Gdx.input.x
        val screenY = Gdx.input.y

        camera.position.x += when {
            screenX < areaX -> -velocity
            screenX >= viewport.worldWidth - areaX -> velocity
            else -> 0f
        }

        camera.position.y += when {
            screenY < areaY -> velocity
            screenY >= viewport.worldHeight - areaY -> -velocity
            else -> 0f
        }
    }

    private fun drawLines() {
        val size = 32f / camera.zoom
        val start = viewport.unproject(Vector2(0f, Gdx.graphics.height.toFloat()))
        val end = viewport.unproject(Vector2(Gdx.graphics.width.toFloat(), 0f))
        val columns = (end.x - start.x).div(size).toInt()
        val rows = (end.y - start.y).div(size).toInt()

        viewport.apply()
        with(shapeRenderer) {
            color = Color.BLACK
            render(camera.combined, ShapeRenderer.ShapeType.Line) {
                for (i in 0..columns) {
                    line(start.x + i * size, start.y, start.x + i * size, end.y)
                }
                for (i in 0..rows) {
                    line(start.x, start.y + i * size, end.x, start.y + i * size)
                }
            }
        }
    }

    private fun drawSelection() {
        val entity = selectedEntity ?: return
        val sprite = entity.sprite ?: return
        val projection = entity.projection ?: camera.combined

        shapeRenderer.render(projection) {
            this.color = Color.GREEN
            shapeRenderer.rect(sprite.x, sprite.y, sprite.width / 2f, sprite.height / 2f, sprite.width, sprite.height, 1f, 1f, sprite.rotation)
        }
    }

    private fun updateFields() {
        zParallaxButton.isVisible = parallaxCheckbox.isChecked
        customParallaxButton.isVisible = parallaxCheckbox.isChecked
        xParallaxField.isVisible = customParallaxButton.isVisible && customParallaxButton.isChecked
        yParallaxField.isVisible = customParallaxButton.isVisible && customParallaxButton.isChecked

        if (hasFocus(null) || selectedEntity == null) {
            xField.text = selectedEntity?.transform?.x?.toString()
            yField.text = selectedEntity?.transform?.y?.toString()
            zField.text = selectedEntity?.transform?.z?.toString()
            xParallaxField.text = selectedEntity?.parallax?.factorX?.toString()
            yParallaxField.text = selectedEntity?.parallax?.factorY?.toString()
            rotationField.text = selectedEntity?.transform?.rotation?.toString()
        }
    }

    fun onDrop(sourcePath: String) {
        assetClass(sourcePath)?.let { assetClass ->
            try {
                val descriptor = AssetDescriptor(sourcePath, assetClass.java)
                assetManager.load(descriptor)
                loadingDndAssets += descriptor
            } catch (e: GdxRuntimeException) {
                error(e) { "Error loading asset" }
            }
        }
    }

    private fun resetFocus() {
        FocusManager.resetFocus(stage)
        selectedEntity = null
    }

    private fun save() {
        val xml = xml("entities") {
            engine.entities.forEach { entity ->
                val texture = entity.sprite?.texture
                val regionPath = texture?.let { assetManager.getAssetFileName(it) }

                "entity" {
                    entity.transform?.let { t ->
                        "x"(t.x.toString())
                        "y"(t.y.toString())
                        "z"(t.z.toString())
                        "rotation"(t.rotation.toString())
                    }
                    if (regionPath != null) {
                        "texture" {
                            "path"(regionPath)
                        }
                    }
                }
            }
        }

        val name = uniqueFilename("xml")
        Gdx.files.local("$EXPORTED_MAPS/$name").writeString(xml.toString(), false)
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewport.update(width, height, false)
    }

    override fun show() {
        super.show()
        inputMultiplexer.addProcessor(inputAdapter)
    }

    override fun hide() {
        super.hide()
        inputMultiplexer.removeProcessor(inputAdapter)
    }

    override fun dispose() {
        super.dispose()
        inputMultiplexer.removeProcessor(inputAdapter)
        stage.dispose()
        assetManager.unloadSafely(dndAssets)
        assetManager.unloadSafely(loadingDndAssets)
        deleteExportedAssets()
    }

    private inner class InputAdapter : com.badlogic.gdx.InputAdapter() {
        override fun scrolled(amount: Int): Boolean {
            zoom(amount * DELTA_ZOOM)
            return true
        }

        override fun keyUp(keycode: Int): Boolean {
            return when (keycode) {
                Input.Keys.FORWARD_DEL -> {
                    if (hasFocus(null)) {
                        selectedEntity?.let { engine.removeEntity(it) }
                        selectedEntity = null
                        true
                    } else {
                        false
                    }
                }
                Input.Keys.ESCAPE -> {
                    if (!hasFocus(null) || selectedEntity != null) {
                        resetFocus()
                    } else {
                        Gdx.app.exit()
                    }
                    true
                }
                else -> false
            }
        }
    }

    companion object {
        private const val DELTA_ZOOM = 0.1f
        private const val MIN_ZOOM = 0.1f
        private const val MAX_ZOOM = 10f

        private val assetDescriptors = listOf(pixelDesc)
    }
}