package de.woed.parallaxgame.editor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.kotcrab.vis.ui.VisUI
import de.woed.parallaxgame.BaseGame
import de.woed.parallaxgame.ContextProvider
import ktx.inject.Context

class EditorGame : BaseGame(), ContextProvider {
    override val context by lazy { Context() }
    private val inputMultiplexer by lazy { InputMultiplexer() }
    override val assetManager by lazy { AssetManager() }

    override var bgColor: Color = Color.BLACK

    override fun create() {
        VisUI.load()
        registerProviders()
        Gdx.input.inputProcessor = inputMultiplexer
    }

    private fun registerProviders() = context.register {
        bindSingleton(this@EditorGame)
        bindSingleton(inputMultiplexer)
        bindSingleton(assetManager)
        bindSingleton<Batch>(SpriteBatch())
        bindSingleton(ShapeRenderer())
    }

    override fun dispose() {
        super.dispose()
        screen?.dispose()
        context.dispose()
        VisUI.dispose()
    }
}