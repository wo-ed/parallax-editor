package de.woed.parallaxgame.editor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import java.text.SimpleDateFormat
import java.util.*
import kotlin.reflect.KClass

val String.extension get() = substringAfterLast(".").toLowerCase()

fun assetClass(path: String): KClass<*>? {
    val extension = path.extension

    return when {
        isTextureFormatSupported(extension) -> Texture::class
        isTextureAtlasFormatSupported(extension) -> TextureAtlas::class
        isEffectFormatSupported(extension) -> ParticleEffect::class
        else -> null
    }
}

private val filenameDateFormat = SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US)
fun uniqueFilename(extension: String) = "${filenameDateFormat.format(Date())}.$extension"

fun deleteExportedAssets(): Boolean = Gdx.files.local(EXPORTS).deleteDirectory()