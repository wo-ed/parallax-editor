package de.woed.parallaxgame.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import de.woed.parallaxgame.demo.DemoGame
import java.awt.Toolkit

object DesktopGameLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val osManager = DesktopOsManager()
        val myGdxGame = DemoGame(osManager)
        val screenSize = Toolkit.getDefaultToolkit().screenSize
        val config = LwjglApplicationConfiguration().apply {
            width = screenSize.width
            height = screenSize.height
        }

        LwjglApplication(myGdxGame, config)
    }
}