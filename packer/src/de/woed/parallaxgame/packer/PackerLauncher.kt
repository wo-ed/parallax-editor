package de.woed.parallaxgame.packer

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.headless.HeadlessApplication
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration

object PackerLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val assetPacker = AssetPacker()
        val config = HeadlessApplicationConfiguration()
        HeadlessApplication(assetPacker, config)
    }
}

class AssetPacker : ApplicationAdapter() {
    override fun create() {
        super.create()
        packSubfoldersToAtlases(INPUT, OUTPUT)
        Gdx.app.exit()
    }

    companion object {
        private const val INPUT = "../../resources/atlastextures"
        private const val OUTPUT = "atlases"
    }
}