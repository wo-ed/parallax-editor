package de.woed.parallaxgame.packer

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.tools.texturepacker.TexturePacker

fun packSubfoldersToAtlases(input: String, output: String, settings: TexturePacker.Settings = defaultTexturePackerSettings(), progress: TexturePacker.ProgressListener? = null) {
    val subfolders = Gdx.files.local(input).list().filter { it.isDirectory }
    subfolders.forEach { packFolderToAtlas(it, output, settings, progress) }
}

fun packFolderToAtlas(folder: FileHandle, output: String, settings: TexturePacker.Settings, progress: TexturePacker.ProgressListener?) {
    if (progress != null) {
        TexturePacker.process(settings, folder.path(), output, folder.name(), progress)
    } else {
        TexturePacker.process(settings, folder.path(), output, folder.name())
    }
}

fun defaultTexturePackerSettings() = TexturePacker.Settings().apply {
    maxWidth = 2048
    maxHeight = 2048
    combineSubdirectories = true
    duplicatePadding = true
}